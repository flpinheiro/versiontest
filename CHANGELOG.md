# Change Log

All notable changes to this project will be documented in this file. See [versionize](https://github.com/versionize/versionize) for commit guidelines.

<a name="0.0.2"></a>
## [0.0.2](https://gitlab.com/flpinheiro/versiontest/-/tags/v0.0.2) (2022-11-22)

<a name="0.0.1"></a>
## [0.0.1](https://gitlab.com/flpinheiro/versiontest/-/tags/v0.0.1) (2022-11-22)

### Features

* update project ([75d55cc](https://gitlab.com/flpinheiro/versiontest/-/commit/75d55ccb19f68128c11ff0e7cccdb44b10a135f9))

